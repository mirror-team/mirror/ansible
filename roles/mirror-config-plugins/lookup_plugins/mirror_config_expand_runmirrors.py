from ansible.errors import AnsibleUndefinedVariable
from ansible.plugins.lookup import LookupBase
from ansible.template import Templar


_marker = object()


class LookupModule(LookupBase):
    def lookup(self, name, hostname, variables, entry=None, default=_marker):
        hostvars = variables['hostvars']

        if not hostname in hostvars:
            if default is not _marker:
                return default
            return None

        v = {
                i: variables[i]
                for i in variables
                if i.startswith('ansible_') or i == 'hostvars' }
        v.update(hostvars[hostname])

        try:
            if entry:
                templ = entry[name]
            else:
                templ = v[name]
        except KeyError:
            if default is not _marker:
                return default
            raise

        templar = Templar(self._loader)
        templar.available_variables = v
        return templar.template(templ, fail_on_undefined=True)

    # Collect all manually added hosts
    def gather_runmirrors_manual(self, config, variables):
        ret = {}

        def config_push(name, dname):
            r = self.lookup('mirror_{}'.format(name), host, variables, default=None)
            if r is not None:
                d.setdefault(dname, r)

        for name, mirror in config.items():
            r = ret.setdefault(name, {})
            for d in mirror.get('runmirrors', ()):
                if 'host' in d:
                    host = d['host']
                    config_push('upstream_push_type', 'type')
                    config_push('upstream_push_url', 'url')
                    config_push('upstream_push_user', 'user')
                    r[host] = d

        return ret

    # Collect all hosts wanting to get triggered
    def gather_runmirrors_want(self, variables):
        ret = {}

        hostname = variables['inventory_hostname']
        groups = variables['groups']
        group_names = variables['group_names']

        def config_get(name, default=None):
            return (self.lookup(name, host, variables, entry=mirror, default=None) or
                    self.lookup('mirror_{}'.format(name), host, variables, default=None) or
                    default)

        def config_push(name, dname, default=None):
            r = config_get(name, default)
            if r is not None:
                d.setdefault(dname, r)

        for host in groups['all']:
            config = self.lookup('mirror_config', host, variables, default={})

            for name, mirror in config.items():
                upstream = mirror.get('upstream')
                name = mirror.get('upstream_name', name)
                push = config_get('upstream_push')

                if ((upstream == hostname or upstream in group_names) and push):
                    d = {}

                    config_push('upstream_push_delay', 'delay',
                            host in groups['debian'] and -300 or 0)
                    config_push('upstream_push_host', 'host', host)
                    config_push('upstream_push_name', 'name')
                    config_push('upstream_push_params', 'params')
                    config_push('upstream_push_type', 'type')
                    config_push('upstream_push_url', 'url')
                    config_push('upstream_push_user', 'user')

                    ret.setdefault(name, {})[host] = d

        return ret

    def run(self, terms, variables=None, **kwargs):
        hostname = variables.get('inventory_hostname')
        if not hostname or not hostname in variables['hostvars']:
            return [{}]

        config = self.lookup('mirror_config', hostname, variables)

        runmirrors_manual = self.gather_runmirrors_manual(config, variables)
        runmirrors_want = self.gather_runmirrors_want(variables)

        # Merge both sets
        for name, manual in runmirrors_manual.items():
            want = runmirrors_want.get(name, [])

            add = {}
            for host in sorted(frozenset(want) - frozenset(manual)):
                delay = int(want[host]['delay'])
                add.setdefault(delay, []).append(want[host])

            delay_last = None
            h = []
            for delay, entries in sorted(add.items()):
                if delay_last is not None:
                    h.append({'special': 'DELAY {}'.format(delay - delay_last)})
                delay_last = delay
                h.extend(sorted(entries, key=lambda e:e['host']))
            config[name].setdefault('runmirrors', [])[0:0] = h

        return [config]
