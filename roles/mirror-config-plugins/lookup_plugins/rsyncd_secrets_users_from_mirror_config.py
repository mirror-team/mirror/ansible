from ansible.errors import AnsibleUndefinedVariable
from ansible.plugins.lookup import LookupBase
from ansible.template import Templar


_marker = object()


class LookupModule(LookupBase):
    def lookup(self, name, hostname, variables, entry=None, default=_marker):
        hostvars = variables['hostvars']

        if not hostname in hostvars:
            if default is not _marker:
                return default
            return None

        v = {
                i: variables[i]
                for i in variables
                if i.startswith('ansible_') or i == 'hostvars' }
        v.update(hostvars[hostname])

        try:
            if entry:
                templ = entry[name]
            else:
                templ = v[name]
        except KeyError:
            if default is not _marker:
                return default
            raise

        templar = Templar(self._loader)
        templar.available_variables = v
        return templar.template(templ, fail_on_undefined=True)

    def run(self, terms, variables=None, **kwargs):
        hostname = variables.get('inventory_hostname')
        if not hostname:
            return []
        groups = variables['groups']
        group_names = variables['group_names']

        ret = {}

        for host in groups['all']:
            config = self.lookup('mirror_config', host, variables, default={})

            for name, mirror in config.items():
                name = mirror.get('path', name)
                upstream = mirror.get('upstream')
                if upstream == hostname or upstream in group_names:
                    ret.setdefault(name, set()).add(host)

        return [{ k: list(v) for k, v in ret.items() }]
