from __future__ import absolute_import, division, print_function

from ansible.errors import AnsibleError


def name(key):
    if key.startswith('debian'):
        return key[6:]
    return '-' + key


class FilterModule(object):
    def filters(self):
        return {
            'mirror_config_name': name,
        }
